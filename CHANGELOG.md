# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.0.0 (2023-01-25)


### Features

* Initial commit ([c9965ea](https://gitlab.com/kikifunstyle/commintlint/commit/c9965eab7a1317a3483f1b3e7cac5643f94717a8))
* **scope:** :bento: test changelog ([6add6b7](https://gitlab.com/kikifunstyle/commintlint/commit/6add6b7b31bb03a119ff6997247c1cf1b9416c08))
* **scope:** :tada: my project ([fd918b0](https://gitlab.com/kikifunstyle/commintlint/commit/fd918b060c35441c2f80250d0f1b410c2be06c6d))
